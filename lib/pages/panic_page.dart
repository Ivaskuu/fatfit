import 'package:flutter/material.dart';
import 'dart:async';

class PanicPage extends StatefulWidget {
  @override
  _PanicPageState createState() => new _PanicPageState();
}

class _PanicPageState extends State<PanicPage> {
  Timer t;
  int _seconds = 20 * 60;
  bool _timerState = false;

  @override
  void initState() {
    super.initState();
    t = Timer.periodic(Duration(seconds: 1), (_) {
      if(_timerState) setState(() => _seconds--);
      if(_seconds <= 0) t.cancel();
    });
  }

  @override
  void dispose() {
    if(t.isActive) t.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'panic',
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          automaticallyImplyLeading: false,
          title: Text('Panic page', style: TextStyle(color: Colors.white, fontSize: 28.0, fontWeight: FontWeight.w700)),
          actions: <Widget>[
            IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(Icons.close, color: Colors.white),
            )
          ],
        ),
        backgroundColor: Colors.red,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Want to eat a snack that you shouldn\'t?\nScientists have proven that waiting some time after an impulsive decision like wanting to eat, can near completley stop it.\n\nWarning: eat if you are really hungry.', style: TextStyle(color: Colors.white)),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox.fromSize(
              size: Size.square(MediaQuery.of(context).size.width / 1.55),
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: SizedBox.fromSize(
                      size: Size.square(MediaQuery.of(context).size.width / 1.55),
                      child: Theme(
                        data: ThemeData(accentColor: Colors.white70),
                        child: CircularProgressIndicator(
                          value: _seconds / (20*60),
                          strokeWidth: 5.0,
                        ),
                      ),
                    )
                  ),
                  Align(
                    alignment: Alignment(0.0, -0.1),
                    child: Icon(Icons.fastfood, color: Colors.white, size: MediaQuery.of(context).size.width / 3.1),
                  ),
                ],
              )
            ),
            Text('${(_seconds / 60).truncate() < 10 ? (_seconds / 60).truncate().toString().padLeft(2, '0') : (_seconds / 60).truncate()}:${(_seconds % 60) < 10 ? (_seconds % 60).toString().padLeft(2, '0') : (_seconds % 60)}', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 80.0)),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 64.0),
              child: Material(
                elevation: 10.0,
                color: Colors.white,
                shadowColor: Colors.black,
                borderRadius: BorderRadius.circular(4.0),
                child: InkWell(
                  onTap: () => setState(() => _timerState = !_timerState),
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(_timerState ? 'STOP TIMER' : 'START TIMER', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                        Icon(_timerState ? Icons.stop : Icons.play_arrow, color: Colors.black),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}