import 'package:flutter/material.dart';
import 'gym_page/gym_page.dart';
import 'dashboard_page/dashboard_page.dart';
import 'exercises_page/exercises_page.dart';
import 'food_page/food_page.dart';
import 'panic_page.dart';

int currentPage = 0;
List<Widget> pages = [
  DashboardPage(),
  GymPage(),
  FoodPage(),
  ExercisesPage()
];

State mainPageState;

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
{
  @override
  Widget build(BuildContext context)
  {
    mainPageState = this;
    return Scaffold(
      body: pages[currentPage],
      bottomNavigationBar: IconOnlyBottomNavigationBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Hero(
        tag: 'panic',
        child: FloatingActionButton(
          onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (_) => PanicPage())),
          mini: true,
          backgroundColor: Colors.red,
          foregroundColor: Colors.white,
          child: Icon(Icons.adjust),
        ),
      ),
    );
  }
}

class IconOnlyBottomNavigationBar extends StatefulWidget {
  @override
  _IconOnlyBottomNavigationBarState createState() => _IconOnlyBottomNavigationBarState();
}

class _IconOnlyBottomNavigationBarState extends State<IconOnlyBottomNavigationBar> {
  @override
  Widget build(BuildContext context) {
    Color blue = Color(0xFF278CFF);

    return Material(
      // color: Color(0xFFE7EBF4),
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 6.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              onPressed: () => setState(() => mainPageState.setState(() => currentPage = 0)),
              iconSize: currentPage == 0 ? 26.0 : 24.0,
              icon: Icon(Icons.dashboard, color: currentPage == 0 ? blue : Colors.black38)
            ),
            IconButton(
              onPressed: () => setState(() => mainPageState.setState(() => currentPage = 1)),
              iconSize: currentPage == 1 ? 26.0 : 24.0,
              icon: Icon(Icons.sort, color: currentPage == 1 ? blue : Colors.black38)
            ),
            IconButton(
              onPressed: () => setState(() => mainPageState.setState(() => currentPage = 2)),
              iconSize: currentPage == 2 ? 26.0 : 24.0,
              icon: Icon(Icons.restaurant_menu, color: currentPage == 2 ? blue : Colors.black38)
            ),
            IconButton(
              onPressed: () => setState(() => mainPageState.setState(() => currentPage = 3)),
              iconSize: currentPage == 3 ? 26.0 : 24.0,
              icon: Icon(Icons.fitness_center, color: currentPage == 3 ? blue : Colors.black38)
            ),
          ],
        )
      )
    );
  }
}