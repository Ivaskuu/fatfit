import 'package:flutter/material.dart';
import 'post_widget.dart';
import '../../models/post.dart';
import '../../models/user.dart';
import 'chat_page/conversations_page.dart';
import 'leaderboard_page.dart';
import 'new_post_page.dart';

User me = User.newUser('Ivascu', 'Adrian', 1999, 95.0, 180, 'ivaskuu', profileImgUrl: 'res/me.png');
User steve = User.newUser('Steve', 'Ganzerla', 1999, 78.0, 174, 'steveganz',
    isTrainer: true, profileImgUrl: 'res/adam.png');

class GymPage extends StatefulWidget {
  @override
  FeedPageState createState() => FeedPageState();
}

class FeedPageState extends State<GymPage> {
  bool wantsToPost = false;
  bool fake = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            floating: true,
            pinned: false,
            elevation: 4.0,
            forceElevated: false,
            backgroundColor: Color(0xFFFAFAFA),
            title: Text('My Gym',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 28.0,
                    fontWeight: FontWeight.w700)),
            actions: <Widget>[
              IconButton(
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (_) => ConversationsPage())),
                icon: Icon(Icons.chat, color: Colors.black),
              ),
              IconButton(
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (_) => LeaderboardPage())),
                icon: Icon(Icons.insert_chart, color: Colors.black),
              ),
            ],
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              fake
                  ? PostWidget(ImagePost.newPost(
                      me, 'res/post1.jpg', 0, PostTag.Bodie,
                      fake: true,
                      content: 'Last workout before going to the sea!'))
                  : Container(),
              PostWidget(ImagePost.newPost(
                  me, 'res/img1.jpg', 37, PostTag.Proggress,
                  content:
                      'Oggi sono dimmagrito 20 kg!!! Adesso mi merito una bella mangiata al giropizza!!')),
              PostWidget(ImagePost.newPost(
                  steve, 'res/img2.jpg', 23, PostTag.Proggress,
                  content: 'Mai dire mai.')),
              PostWidget(Post.award(
                  User('Jessica', 'Charles', null, null, null, null, null, null,
                      null, null, null, null, profileImgUrl: 'res/selfie.jpeg'),
                  'No pain, no gain!',
                  6)),
              PostWidget(Post.award(steve,
                  'Ho appena guadagnato il Trofeo \'Perdere 10kg\'!', 49)),
            ]),
          ),
          SliverToBoxAdapter(
            child: SizedBox(height: 64.0),
          )
        ],
      ),
      floatingActionButton: !wantsToPost
          ? FloatingActionButton(
              heroTag: null,
              onPressed: () {
                setState(() {
                  wantsToPost = true;
                });
              },
              backgroundColor: Colors.black,
              child: Icon(Icons.add, color: Colors.white),
            )
          : null,
      bottomNavigationBar: wantsToPost
          ? Material(
              color: Colors.white,
              elevation: 10.0,
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Colors.black,
                          child: Icon(Icons.text_fields, color: Colors.white),
                        ),
                        SizedBox(height: 8.0),
                        Text('Text post')
                      ],
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          wantsToPost = false;
                          fake = true;
                        });
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (_) => NewPostPage()),
                        );
                      },
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Colors.black,
                            child: Icon(Icons.image, color: Colors.white),
                          ),
                          SizedBox(height: 8.0),
                          Text('Image post')
                        ],
                      ),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: Colors.black,
                          child: Icon(Icons.videocam, color: Colors.white),
                        ),
                        SizedBox(height: 8.0),
                        Text('Video post')
                      ],
                    ),
                  ],
                ),
              ),
            )
          : null,
    );

    /* ListView
      (
        scrollDirection: Axis.vertical,
        children: <Widget>
        [
        ],
      ),
      floatingActionButton: FloatingActionButton
      (
        onPressed: () => null,
        child: Icon(Icons.add),
      ),
    ); */
  }
}
