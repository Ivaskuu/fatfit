import 'package:flutter/material.dart';
import 'post_widget.dart';
import '../../models/post.dart';
import '../../models/user.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class NewPostPage extends StatefulWidget {
  @override
  _NewPostPageState createState() => _NewPostPageState();
}

class _NewPostPageState extends State<NewPostPage> {
  File imageFile;

  @override
  void initState() {
    super.initState();
    pickImage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Text(
          'Add new image',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 22.0,
            color: Colors.black,
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.close, color: Colors.black54),
          )
        ],
      ),
      body: imageFile == null
          ? Container()
          : ListView(
              padding: EdgeInsets.all(16.0),
              children: <Widget>[
                InkWell(
                  onTap: () => pickImage(),
                  child: Container(
                      margin: EdgeInsets.symmetric(vertical: 4.0),
                      child: Material(
                          elevation: 6.0,
                          borderRadius: BorderRadius.circular(12.0),
                          child: SizedBox.fromSize(
                              size: Size.fromHeight(180.0),
                              child:
                                  Image.file(imageFile, fit: BoxFit.cover)))),
                ),
                sectionTitle('Image infos'),
                TextField(
                    decoration:
                        InputDecoration(hintText: 'Your image description')),
                SizedBox(height: 16.0),
                Align(
                  alignment: Alignment.centerLeft,
                  child: FlatButton.icon(
                      onPressed: () {},
                      icon: Icon(Icons.person_pin_circle, color: Colors.pink),
                      label: Text('TAG PEOPLE',
                          style: TextStyle(color: Colors.pink))),
                ),
                sectionTitle('Privacy'),
                SizedBox(height: 8.0),
                SwitchListTile(
                    onChanged: (_) {},
                    value: true,
                    activeColor: Colors.pink,
                    title: Text('Enable comments')),
                SizedBox(height: 0.0),
                ListTile(
                    selected: true,
                    leading: Icon(Icons.public, color: Colors.pink),
                    title: Text('Public',
                        style: TextStyle(
                            color: Colors.pink, fontWeight: FontWeight.bold))),
                ListTile(
                    leading: Icon(Icons.people), title: Text('Friends only')),
                ListTile(leading: Icon(Icons.lock), title: Text('Only me')),
                SizedBox(height: 64.0),
              ],
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pop(context),
        child: Icon(Icons.send),
        backgroundColor: Colors.pink,
      ),
    );
  }

  Widget sectionTitle(String text) {
    return Column(
      children: <Widget>[
        SizedBox(height: 32.0),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(text,
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.pink,
                    fontSize: 14.0))),
      ],
    );
  }

  void pickImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      imageFile = image;
    });
  }
}
