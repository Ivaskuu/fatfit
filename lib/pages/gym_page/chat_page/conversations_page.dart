import 'package:flutter/material.dart';
import 'conversation_widget.dart';

class ConversationsPage extends StatefulWidget
{
  @override
  _ConversationsPageState createState() => _ConversationsPageState();
}

class _ConversationsPageState extends State<ConversationsPage>
{
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.close),
          color: Colors.black,
        ),
        title: Text('My Chats', style: TextStyle(color: Colors.black, fontSize: 28.0, fontWeight: FontWeight.w700)),
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget> [
          ConversationWidget('Steve Ganz', 'Ci alleniamo insieme questo pomeriggio?'),
          ConversationWidget('Allenatore', 'Va bene!'),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.black,
        child: Icon(Icons.person_add),
      ),
    );
  }
}