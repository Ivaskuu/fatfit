import 'package:flutter/material.dart';

class ChatBubbleWidget extends StatelessWidget {
  final bool leftSide;
  final String msg;
  ChatBubbleWidget(this.leftSide, this.msg);

  @override
  Widget build(BuildContext context)
  {
    if(leftSide)
    {
      return Container
      (
        margin: EdgeInsets.only(left: 8.0, right: 64.0, bottom: 8.0),
        child: Stack
        (
          children: <Widget>
          [
            Padding
            (
              padding: const EdgeInsets.only(top: 24.0, left: 26.0),
              child: Material
              (
                elevation: 6.0,
                shadowColor: Colors.blueAccent.withOpacity(0.3),
                borderRadius: BorderRadius.circular(24.0),
                color: Colors.white,
                child: Container
                (
                  margin: EdgeInsets.only(top: 12.0, bottom: 12.0, left: 16.0, right: 16.0),
                  child: Text(msg, style: TextStyle(color: Colors.black)),
                ),
              ),
            ),
            Material
            (
              elevation: 10.0,
              shape: CircleBorder(),
              child: CircleAvatar
              (
                radius: 20.0,
                backgroundImage: AssetImage('res/me.png'),
              ),
            ),
          ],
        ),
      );
    }
    else
    {
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>
        [
          Flexible
          (
            child: Container
            (
              margin: EdgeInsets.only(left: 64.0, right: 8.0, bottom: 4.0),
              child: Padding
              (
                padding: const EdgeInsets.only(top: 8.0, left: 35.0),
                child: Material
                (
                  elevation: 10.0,
                  shadowColor: Color(0x88FFFFFF),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(24.0),
                    topLeft: Radius.circular(24.0),
                    bottomRight: Radius.circular(24.0),
                  ),
                  color: Color(0xFF278CFF),
                  child: Container
                  (
                    margin: EdgeInsets.only(top: 12.0, bottom: 12.0, left: 16.0, right: 16.0),
                    child: Text(msg, style: TextStyle(color: Colors.white)),
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    }
  }
}