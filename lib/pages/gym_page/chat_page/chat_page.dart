import 'package:flutter/material.dart';
import 'chat_bubble_widget.dart';
import 'write_message_widget.dart';
import '../../profile_page/profile_page.dart';

class ChatPage extends StatefulWidget
{
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage>
{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Steve Ganz', style: TextStyle(color: Colors.black, fontSize: 22.0, fontWeight: FontWeight.w700)),
            Text('Online 2 minuti fa', style: TextStyle(color: Colors.black38, fontSize: 14.0))
          ],
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.close),
          color: Colors.black38,
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: InkWell(
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) => ProfilePage())),
              child: Material(
                shape: CircleBorder(),
                elevation: 4.0,
                shadowColor: Colors.black,
                child: CircleAvatar(
                  backgroundImage: AssetImage('res/me.png'),
                ),
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView
            (
              reverse: true,
              children: <Widget>
              [
                ChatBubbleWidget(true, 'Ci alleniamo insieme questo pomeriggio?'),
                ChatBubbleWidget(true, 'Grande!!'),
                ChatBubbleWidget(false, 'Molto bene. Mi sono iscritto alla palestra finalmente :)'),
                ChatBubbleWidget(true, 'Bene, tu?'),
                ChatBubbleWidget(false, 'Come stai?'),
                ChatBubbleWidget(false, 'Ciao Steve!'),
              ],
            ),
          ),
          WriteMessageWidget()
        ],
      ),
    );
  }
}