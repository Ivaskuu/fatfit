import 'package:flutter/material.dart';

class WriteMessageWidget extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row
      (
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>
        [
          Flexible(
            child: Container(
              margin: EdgeInsets.only(top: 12.0, bottom: 12.0, left: 16.0, right: 16.0),
              child: TextField(
                maxLines: null,
                autocorrect: false,
                decoration: InputDecoration.collapsed(
                  hintText: 'Write here your message...'
                ),
              ),
            ),
          ),
          FloatingActionButton(
            onPressed: () => null,
            mini: true,
            backgroundColor: Colors.black,
            child: Icon(Icons.send),
          )
        ],
      ),
    );
  }
}