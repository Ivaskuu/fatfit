import 'package:flutter/material.dart';
import 'chat_page.dart';

class ConversationWidget extends StatelessWidget
{
  final String name;
  final String lastMsg;

  ConversationWidget(this.name, this.lastMsg);

  @override
  Widget build(BuildContext context)
  {
    return InkWell
    (
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) => ChatPage())),
      child: Container
      (
        margin: EdgeInsets.symmetric(vertical: 12.0, horizontal: 12.0),
        child: Row
        (
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>
          [
            Material
            (
              shape: CircleBorder(),
              elevation: 8.0,
              shadowColor: Colors.black38,
              color: Colors.grey,
              child: SizedBox.fromSize(
                size: Size.fromRadius(30.0),
                child: Image.asset('res/me.png'),
              ),
            ),
            Padding(padding: EdgeInsets.only(right: 12.0)),
            Expanded
            (
              child: Column
              (
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>
                [
                  Text(name, style: Theme.of(context).textTheme.title),
                  Text(lastMsg, overflow: TextOverflow.ellipsis, maxLines: 2)
                ],
              ),
            ),
            Text('16:48')
          ],
        ),
      ),
    );
  }
}