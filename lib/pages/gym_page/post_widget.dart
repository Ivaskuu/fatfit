import 'package:flutter/material.dart';
import '../../models/post.dart';
import '../profile_page/profile_page.dart';

LinearGradient awardGradient = LinearGradient(
    colors: [Color(0xFFFE8077), Color(0xFFB22B5B)],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight);

class PostWidget extends StatefulWidget {
  final Post post;
  PostWidget(this.post) : super(key: Key(post.content.toString()));

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.post is ImagePost) {
      return Container(
        margin: EdgeInsets.only(top: 4.0, bottom: 16.0),
        child: Material(
          color: Colors.white,
          elevation: 3.0,
          shadowColor: Colors.black87,
          child: Column(
            children: <Widget>[
              InkWell(
                onTap: () => Navigator.push(
                    context, MaterialPageRoute(builder: (_) => ProfilePage())),
                child: ListTile(
                  leading: CircleAvatar(
                      backgroundColor: Colors.black12,
                      backgroundImage:
                          AssetImage(widget.post.user.profileImgUrl)),
                  title: Text(
                      '${widget.post.user.firstName} ${widget.post.user.lastName}',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
                  subtitle: Text(widget.post.fake ? 'now' : '2 mins ago'),
                  trailing: IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.more_horiz),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 12.0, right: 12.0, bottom: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(widget.post.content, textAlign: TextAlign.start),
                    Padding(padding: EdgeInsets.only(bottom: 8.0)),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 4.0),
                      child: Material(
                        elevation: 6.0,
                        borderRadius: BorderRadius.circular(12.0),
                        child: SizedBox.fromSize(
                          size: Size.fromHeight(180.0),
                          child: Image.asset((widget.post as ImagePost).imgUrl,
                              fit: BoxFit.cover),
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 8.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        FlatButton.icon(
                          onPressed: () {},
                          icon: Icon(Icons.share),
                          label: Text('Share'),
                        ),
                        FlatButton.icon(
                          onPressed: () {},
                          icon: Icon(Icons.comment),
                          label: Text('0'),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              if (widget.post.user.firstName != 'Ivascu')
                                widget.post.likes++;
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 14.0, vertical: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(widget.post.likes.toString(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0)),
                                Padding(padding: EdgeInsets.only(right: 8.0)),
                                Text('👏', style: TextStyle(fontSize: 24.0)),
                              ],
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container(
        margin: EdgeInsets.only(top: 4.0, bottom: 16.0),
        child: Material(
          color: Colors.white,
          elevation: 3.0,
          shadowColor: Colors.black87,
          child: Column(
            children: <Widget>[
              InkWell(
                onTap: () => Navigator.push(
                    context, MaterialPageRoute(builder: (_) => ProfilePage())),
                child: ListTile(
                  leading: CircleAvatar(
                      backgroundColor: Colors.black12,
                      backgroundImage:
                          AssetImage(widget.post.user.profileImgUrl)),
                  title: Text(
                      '${widget.post.user.firstName} ${widget.post.user.lastName}',
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
                  subtitle: Text('2 mins ago'),
                  trailing: IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.more_horiz),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 12.0, right: 12.0, bottom: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(widget.post.content, textAlign: TextAlign.start),
                    Padding(padding: EdgeInsets.only(bottom: 16.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        FlatButton.icon(
                          onPressed: () {},
                          icon: Icon(Icons.share),
                          label: Text('Share'),
                        ),
                        FlatButton.icon(
                          onPressed: () {},
                          icon: Icon(Icons.comment),
                          label: Text('1'),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              widget.post.likes++;
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 14.0, vertical: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(widget.post.likes.toString(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0)),
                                Padding(padding: EdgeInsets.only(right: 8.0)),
                                Text('👏', style: TextStyle(fontSize: 24.0)),
                              ],
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  IconData postTagToIcon(PostTag tag) {
    switch (tag) {
      case PostTag.Bodie:
        return Icons.accessibility_new;
        break;
      case PostTag.Selfie:
        return Icons.face;
        break;
      case PostTag.Trophie:
        return Icons.grade;
        break;
      case PostTag.Proggress:
        return Icons.trending_up;
        break;
      case PostTag.Question:
        return Icons.help;
        break;
      case PostTag.Cook:
        return Icons.restaurant_menu;
        break;
      case PostTag.Motivational:
        return Icons.whatshot;
        break;
      default:
        return null;
        break;
    }
  }
}
