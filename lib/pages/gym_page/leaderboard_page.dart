import 'package:flutter/material.dart';
import '../../models/user.dart';

class LeaderboardPage extends StatelessWidget {
  final List<User> users = [
    new User.newUser('Stephen', 'Quartarone', null, null, null, null),
    new User.newUser('Adrian', 'Ivascu', null, null, null, null),
    new User.newUser('Davide', 'Addesi', null, null, null, null),
    new User.newUser('Steve', 'Ganz', null, null, null, null),
    new User.newUser('Simone', 'Scino', null, null, null, null),
    new User.newUser('Davide', 'Rossi', null, null, null, null),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Text('Leaderboard',
            style: TextStyle(
                color: Colors.black,
                fontSize: 28.0,
                fontWeight: FontWeight.w700)),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back, color: Colors.black),
        ),
      ),
      body: ListView.builder(
        itemBuilder: (_, int i) => i == 0
            ? LeaderboardPodium()
            : i == 1
                ? Divider()
                : i >= 2 ? LeadeboardTile(i, users[i]) : Container(),
        itemCount: users.length,
      ),
    );
  }
}

class LeaderboardPodium extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      elevation: 4.0,
      shadowColor: Colors.black54,
      child: SizedBox.fromSize(
        size: Size.fromHeight(200.0),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      SizedBox.fromSize(
                        size: Size.square(60.0),
                        child: Material(
                          elevation: 4.0,
                          shadowColor: Colors.black54,
                          shape: CircleBorder(),
                          child: Image.asset('res/me.png'),
                        ),
                      ),
                      Align(
                          alignment: Alignment.bottomCenter,
                          child: Material(
                            elevation: 4.0,
                            shape: CircleBorder(),
                            color: Colors.grey,
                            child: Container(
                              margin: EdgeInsets.all(6.0),
                              child: Text('2',
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ))
                    ],
                  ),
                  SizedBox(height: 8.0),
                  Text('Ivascu Adrian',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w700))
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      SizedBox.fromSize(
                        size: Size.square(100.0),
                        child: Material(
                          elevation: 4.0,
                          shadowColor: Colors.black54,
                          shape: CircleBorder(),
                          child: Image.asset('res/me.png'),
                        ),
                      ),
                      Align(
                          alignment: Alignment.bottomCenter,
                          child: Material(
                            elevation: 4.0,
                            shape: CircleBorder(),
                            color: Colors.yellow,
                            child: Container(
                              margin: EdgeInsets.all(8.0),
                              child: Text('1',
                                  style: TextStyle(color: Colors.black)),
                            ),
                          ))
                    ],
                  ),
                  SizedBox(height: 8.0),
                  Text('Ivascu Adrian',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w700))
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      SizedBox.fromSize(
                        size: Size.square(60.0),
                        child: Material(
                          elevation: 4.0,
                          shadowColor: Colors.black54,
                          shape: CircleBorder(),
                          child: Image.asset('res/me.png'),
                        ),
                      ),
                      Align(
                          alignment: Alignment.bottomCenter,
                          child: Material(
                            elevation: 4.0,
                            shape: CircleBorder(),
                            color: Colors.orange,
                            child: Container(
                              margin: EdgeInsets.all(6.0),
                              child: Text('3',
                                  style: TextStyle(color: Colors.white)),
                            ),
                          ))
                    ],
                  ),
                  SizedBox(height: 8.0),
                  Text('Ivascu Adrian',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w700))
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LeadeboardTile extends StatelessWidget {
  final int pos;
  final User user;
  LeadeboardTile(this.pos, this.user);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: CircleAvatar(
              child: Text((pos + 2).toString(),
                  style: TextStyle(color: Colors.white))),
          title: Text(user.firstName + ' ' + user.lastName,
              style: TextStyle(fontSize: 20.0)),
          subtitle: Text('${(100 - (pos + 2) * 7.3).round()} punti'),
          trailing: SizedBox.fromSize(
            size: Size.square(40.0),
            child: Material(
              elevation: 4.0,
              shadowColor: Colors.black54,
              shape: CircleBorder(),
              child: Image.asset('res/me.png'),
            ),
          ),
        ),
        Divider()
      ],
    );
  }
}
