import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import '../../models/food.dart';

class SingleFoodPage extends StatefulWidget {
  final Food food;
  final bool fromSearch;
  SingleFoodPage(this.food, {this.fromSearch});

  @override
  _SingleFoodPageState createState() => new _SingleFoodPageState();
}

class _SingleFoodPageState extends State<SingleFoodPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.white,
            elevation: 4.0,
            forceElevated: false,
            floating: true,
            snap: true,
            leading: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(Icons.arrow_back, color: Colors.black38),
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.edit, color: Colors.black),
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.delete, color: Colors.black),
              ),
            ],
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Hero(
                  tag: widget.food.name + widget.food.kcalGr.toString(),
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                    child: Material(
                      elevation: 6.0,
                      borderRadius: BorderRadius.circular(12.0),
                      child: SizedBox.fromSize(
                        size: Size.fromHeight(200.0),
                        child: widget.food.img != null
                          ? Image.asset('res/${widget.food.img}', fit: BoxFit.cover)
                          : Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [ Color(0xFF4b6cb7), Color(0xFF182848) ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight
                              )
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.fastfood, color: Colors.white, size: 48.0),
                                SizedBox(height: 16.0),
                                Text('No image', style: TextStyle(color: Colors.white))
                              ]
                            )
                          ),
                      ),
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(bottom: 16.0)),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
                  child: Text(widget.food.name, maxLines: 2, overflow: TextOverflow.fade, style: Theme.of(context).textTheme.title.copyWith(fontSize: 32.0)),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20.0),
                  child: Text('Dettagli', style: TextStyle(fontWeight: FontWeight.w700, color: Colors.black45, fontSize: 16.0)),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 32.0),
                  child: AnimatedCircularChart
                  (
                    percentageValues: true,
                    chartType: CircularChartType.Radial,
                    size: Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.width - 32.0),
                    holeRadius: 96.0,
                    holeLabel: '${widget.food.kcalGr.round()} cal',
                    labelStyle: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.w600),
                    initialChartData: [
                      CircularStackEntry([
                        CircularSegmentEntry(((widget.food.fats * 9) / widget.food.kcalGr) * 100, Colors.green),
                        CircularSegmentEntry(((widget.food.proteins * 4) / widget.food.kcalGr) * 100, Colors.red),
                        CircularSegmentEntry(((widget.food.carbs * 4) / widget.food.kcalGr) * 100, Colors.blue),
                      ])
                    ],
                  ),
                ),
                /// Chart details
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 28.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 4.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox.fromSize(
                              size: Size.square(16.0),
                              child: Container(
                                color: Colors.green,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 8.0),
                            ),
                            Text('Grassi (' + (widget.food.fats * 9 / widget.food.kcalGr * 100).round().toString() + '%)'),
                          ],
                        ),
                      ),
                      Padding
                      (
                        padding: EdgeInsets.symmetric(vertical: 4.0),
                        child: Row
                        (
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>
                          [
                            SizedBox.fromSize(
                              size: Size.square(16.0),
                              child: Container(
                                color: Colors.red,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 8.0),
                            ),
                            Text('Proteine (' + (widget.food.proteins * 4 / widget.food.kcalGr * 100).round().toString() + '%)'),
                          ],
                        ),
                      ),
                      Padding
                      (
                        padding: EdgeInsets.symmetric(vertical: 4.0),
                        child: Row
                        (
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>
                          [
                            SizedBox.fromSize(
                              size: Size.square(16.0),
                              child: Container(
                                color: Colors.blue,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 8.0),
                            ),
                            Text('Carboidrati (' + (widget.food.carbs * 4 / widget.food.kcalGr * 100).round().toString() + '%)'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 32.0)
              ]
            ),
          )
        ],
      ),
      floatingActionButton: widget.fromSearch != null ? FloatingActionButton.extended(
        onPressed: () {
          Navigator.pop(context);
          Navigator.pop(context);
        },
        backgroundColor: Colors.black,
        icon: Icon(Icons.add_circle_outline),
        label: Text('Add food'),
      ) : null,
    );
  }
}