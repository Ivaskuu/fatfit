import 'package:flutter/material.dart';
import 'single_food_page.dart';
import '../../models/food.dart';

class FoodSearchPage extends StatefulWidget {
  final String barcode;
  FoodSearchPage({this.barcode});

	@override
  _FoodSearchPageState createState() => _FoodSearchPageState();
}

class _FoodSearchPageState extends State<FoodSearchPage> {
  @override
  Widget build(BuildContext context) {
    TextEditingController _controller = new TextEditingController(text: widget.barcode ?? '');

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(0.0, 0.0),
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [ Color(0xFF02C78D), Color(0xFF00FBB1) ],
              begin: Alignment(-1.0, -1.0),
              end: Alignment(1.0, 1.0),
            )
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          SizedBox.fromSize(
            size: Size.fromHeight(190.0),
            child: Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 42.0),
                  child: Align(
                    alignment: FractionalOffset.topCenter,
                    child: Material(
                      elevation: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [ Color(0xFF02C78D), Color(0xFF00FBB1) ],
                            begin: Alignment(-1.0, -1.0),
                            end: Alignment(1.0, 1.0),
                          )
                        )
                      )
                    ),
                  )
                ),
                Align // Appbar with the close iconbutton
                (
                  alignment: FractionalOffset.topCenter,
                  child: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0.0,
                    leading: IconButton(
                      onPressed: () => Navigator.of(context).pop(),
                      icon: Icon(Icons.close, color: Colors.white70)
                    ),
                  ),
                ),
                SizedBox.expand(
                  child: Container(
                    margin: EdgeInsets.only(bottom: 56.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.fastfood, color: Colors.white),
                        Padding(padding: EdgeInsets.only(right: 8.0)),
                        Text('SEARCH', style: Theme.of(context).textTheme.title.copyWith(color: Colors.white, fontSize: 26.0, fontWeight: FontWeight.w600)),
                      ],
                    ),
                  ),
                ),
                Container // Search textField
                (
                  margin: EdgeInsets.all(16.0),
                  child: Align
                  (
                    alignment: FractionalOffset.bottomCenter,
                    child: Material
                    (
                      elevation: 8.0,
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(2.0),
                      child: Container
                      (
                        margin: EdgeInsets.all(16.0),
                        child: Row
                        (
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>
                          [
                            Container(margin: EdgeInsets.only(left: 8.0, right: 16.0), child: Icon(Icons.search)),
                            Expanded(
                              child: TextField
                              (
                                controller: _controller,
                                autocorrect: false,
                                decoration: InputDecoration.collapsed(
                                  hintText: "What do you want to search for?",
                                ),
                                onChanged: (_) => setState(() {}),
                              )
                            )
                          ]
                        )
                      ),
                    ),
                  )
                )
              ]
            )
          ),
          Container(
            margin: EdgeInsets.only(left: 10.0, top: 10.0),
            child: Text('Results', style: TextStyle(fontWeight: FontWeight.w700, color: Colors.black45, fontSize: 16.0)),
          ),
          SearchResultCard(),
          SearchResultCard(),
          SearchResultCard(),
          SearchResultCard(),
          SearchResultCard(),
        ],
      )
    );
  }
}

class SearchResultCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
      child: Material(
        color: Colors.white,
        elevation: 4.0,
        shadowColor: Colors.black38,
        child: InkWell(
          onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) => SingleFoodPage(Food('Insalata con salmone', 229.0), fromSearch: true))),
          child: Container
          (
            margin: EdgeInsets.all(4.0),
            child: ListTile(
              title: Text('Nome cibo', style: TextStyle(fontWeight: FontWeight.w400, fontSize: 20.0)),
              subtitle: Text('100g / 250 cal'),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('257', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0)),
                  Padding(padding: EdgeInsets.only(bottom: 4.0)),
                  Text('kcal'),
                ],
              ),
            )
          ),
        )
      ),
    );
  }
}