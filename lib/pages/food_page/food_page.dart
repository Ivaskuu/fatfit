import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'dart:async';
import 'single_food_page.dart';
import 'food_search_page.dart';
import '../../models/food.dart';

class FoodPage extends StatefulWidget {
  @override
  _FoodPageState createState() => new _FoodPageState();
}

class _FoodPageState extends State<FoodPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.white,
            elevation: 2.0,
            forceElevated: false,
            title: Text('My Nutrition', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 24.0)),
            actions: <Widget>[
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.event, color: Colors.black),
              ),
            ],
            pinned: false,
            floating: true,
          ),
          SliverToBoxAdapter(
            child: CaloriesTodayBar(),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                FoodTile(Food('McMenu Gran Crispy McBacon', 481.0, img: 'f1.jpg')),
                FoodTile(Food('Milkshake vaniglia e arancia', 573.0, img: 'f2.jpg')),
                FoodTile(Food('Insalata con salmone', 229.0, img: 'f3.jpg')),
              ]
            ),
          )
        ],
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          FloatingActionButton(
            heroTag: 'f1',
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (_) => FoodSearchPage(barcode: 'Mela rossa'))),
            mini: true,
            backgroundColor: Colors.black,
            child: Icon(Icons.add, color: Colors.white),
          ),
          Padding(padding: EdgeInsets.only(bottom: 4.0)),
          FloatingActionButton(
            heroTag: 'f2',
            onPressed: () => scan(),
            backgroundColor: Colors.black,
            child: Icon(Icons.camera_alt, color: Colors.white),
          ),
        ],
      )
    );
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      Navigator.push(context, MaterialPageRoute(builder: (_) => SingleFoodPage(Food('Yogurt AlpiYo Fragole', 100.0, weight: 125, img: 'gelato.jpg'))));
    }
    catch (e) {
      print('Errore: ' + e.toString());
    }
    /* on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      }
      else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    }
    on FormatException{
      setState(() => this.barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    }
    catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    } */
  }
}

class CaloriesTodayBar extends StatefulWidget {
  @override
  _CaloriesTodayBarState createState() => new _CaloriesTodayBarState();
}

class _CaloriesTodayBarState extends State<CaloriesTodayBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16.0, right: 16.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text('You can still eat'),
              Text(' 1398 ', style: TextStyle(fontWeight: FontWeight.bold)),
              Text(' calories'),
            ],
          ),
          Padding(padding: EdgeInsets.only(bottom: 8.0)),
          Theme(
            data: ThemeData(accentColor: Colors.black),
            child: LinearProgressIndicator(
              backgroundColor: Colors.black12,
              value: 0.17,
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 8.0))
        ],
      ),
    );
  }
}

class FoodTile extends StatelessWidget {
  final Food food;
  FoodTile(this.food);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
      child: InkWell(
        onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) => SingleFoodPage(food))),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 8.0),
              padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Container(
                      padding: EdgeInsets.only(right: 12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(food.name, maxLines: 2, overflow: TextOverflow.fade, style: Theme.of(context).textTheme.title),
                          Padding(padding: EdgeInsets.only(bottom: 2.0)),
                          Text('13:48 5/6/2018', style: Theme.of(context).textTheme.caption),
                        ],
                      ),
                    ),
                  ),
                  Text('${food.kcalGr.round()}cal', style: Theme.of(context).textTheme.title),
                ],
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 8.0)),
            Hero(
              tag: food.name + food.kcalGr.toString(),
              child: Material(
                elevation: 6.0,
                borderRadius: BorderRadius.circular(12.0),
                child: SizedBox.fromSize(
                  size: Size.fromHeight(200.0),
                  child: Image.asset('res/${food.img}', fit: BoxFit.cover),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}