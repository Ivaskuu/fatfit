import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart' as sparkChart;
import 'steps_chart.dart';
import 'package:location/location.dart';
import 'package:latlong/latlong.dart' as latlong;
import '../profile_page/p2.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => new _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  List<Map<StaggeredTile, Widget>> tiles;

  @override
  void initState() {
    super.initState();

    tiles = [
      /// Calories warning
      {
        StaggeredTile.count(3, 1): _buildGridTile(
          LayoutBuilder(builder: (context, constraint) {
            return new Row(
              children: <Widget>[
                AnimatedCircularChart
                (
                  percentageValues: true,
                  chartType: CircularChartType.Radial,
                  size: Size(constraint.maxHeight, constraint.maxHeight),
                  // holeRadius: 48.0,
                  holeLabel: '473\ncal left',
                  labelStyle: TextStyle(color: Colors.white),
                  initialChartData: [
                    CircularStackEntry([
                      CircularSegmentEntry(70.0, Colors.white),
                    ])
                  ],
                ),
                Padding(padding: EdgeInsets.only(right: 12.0)),
                Flexible(child: Text('Attention, you are getting close to the daily limit of calories.', textAlign: TextAlign.left, style: TextStyle(color: Colors.white)))
              ],
            );
          }),
          gradient: LinearGradient(
            colors: [ Colors.red, Colors.pink ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight
          )
        )
      },
      /// Notifications expanded
      {
        StaggeredTile.count(3, 1): _buildGridTile(
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.notifications, color: Color(0xFF278CFF), size: 38.0),
              Text('You have 3 new notifications'),
            ],
          ),
          padding: false
        ),
      },
      /// Add food simple button
      {
        StaggeredTile.count(1, 1): _buildGridTile(
          InkWell(
            onTap: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Icon(Icons.fastfood),
                Text('Add a meal'),
              ],
            ),
          ),
          color: Colors.white
        )
      },
      /// Take a photo of a meal
      {
        StaggeredTile.count(2, 1): _buildGridTile(
          InkWell(
            onTap: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Icon(Icons.camera_alt),
                Text('Take a photo of a meal'),
              ],
            ),
          ),
          color: Colors.white
        )
      },
      /// Weight with chart
      {
        StaggeredTile.count(3, 2): _buildGridTile(
          InkWell(
            onTap: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Weight', style: TextStyle(color: Colors.white)),
                    Text('92.9kg', style: TextStyle(color: Colors.white, fontSize: 28.0, fontWeight: FontWeight.w700))
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: sparkChart.Sparkline
                    (
                      data: [98.0, 97.8, 98.4, 96.2, 96.6, 95.9],
                      lineWidth: 4.0,
                      lineColor: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          ),
          gradient: LinearGradient(
            colors: [ Colors.purple, Colors.pink ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight
          )
        )
      },
      /// Steps
      {
        StaggeredTile.count(3, 1): _buildGridTile(
          Row(
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Steps today', style: TextStyle(color: Colors.white)),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('8096', style: TextStyle(color: Colors.white, fontSize: 36.0, fontWeight: FontWeight.bold)),
                      Text('3.23 km', style: TextStyle(color: Colors.white))
                    ],
                  ),
                ],
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(left: 96.0, top: 16.0),
                  child: StepsChart(daysToShow: null),
                )
              ),
            ],
          ),
          gradient: LinearGradient(
            colors: [ Color(0xFF021B79), Color(0xFF0575E6) ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight
          )
        )
      },
      /* /// No notifications
      {
        StaggeredTile.count(1, 1): _buildGridTile(
          InkWell(
            onTap: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.notifications_none, size: 32.0),
              ],
            ),
          ),
          color: Colors.white
        )
      }, */
      /// Calories with circle graph
      {
        StaggeredTile.count(1, 1): _buildGridTile(
          InkWell(
            onTap: () {},
            child: new LayoutBuilder(builder: (context, constraint) {
              return AnimatedCircularChart
              (
                percentageValues: true,
                chartType: CircularChartType.Radial,
                size: constraint.biggest,
                holeRadius: 32.0,
                holeLabel: '1300\ncal left',
                labelStyle: TextStyle(color: Colors.white),
                initialChartData: [
                  CircularStackEntry([
                    CircularSegmentEntry(70.0, Colors.white),
                  ])
                ],
              );
            }),
          ),
          gradient: LinearGradient(
            colors: [ Colors.blue, Colors.indigo ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight
          )
        )
      },
      /// Allenatore
      {
        StaggeredTile.count(1, 1): _buildGridTile(
          InkWell(
            onTap: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Material(
                  elevation: 6.0,
                  shape: CircleBorder(),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('res/me.png'),
                  ),
                ),
                Text('Allenatore'),
              ],
            ),
          ),
          color: Colors.white
        )
      },
      /// Shortcut to a friend
      /// Add something else
    ];

    try {
      Location().getLocation.then((position) {
        if(position != null && latlong.Distance().distance(new latlong.LatLng(position['latitude'], position['longitude']), new latlong.LatLng(45.58414199931135, 9.4832181930542)) <= 100) {
          setState(() {
            tiles.insert(0, 
            {
              StaggeredTile.count(3, 2): _buildGridTile(
                Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    Image.asset('res/login.jpeg', fit: BoxFit.cover),
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [ Colors.transparent, Colors.black ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter
                        )
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        margin: EdgeInsets.all(16.0),
                        child: Text('Start training', style: TextStyle(color: Colors.white, fontSize: 32.0, fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ), padding: false
              )
            });
          });
        }
      });
    }
    catch(_) {
      print('Errore posizione');
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white, //Color(0xFFE7EBF4),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 84.0,
            flexibleSpace: Container(
              color: Colors.white,
              padding: EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0,),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Ivascu Adrian', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 32.0)),
                      Text('You are doing great!', style: TextStyle(color: Colors.black)),
                    ],
                  ),
                  Material(
                    elevation: 6.0,
                    shape: CircleBorder(),
                    child: InkWell(
                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) => ProfilePage())),
                      child: CircleAvatar(
                        backgroundImage: AssetImage('res/me.png'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(horizontal: 4.0),
            sliver: SliverStaggeredGrid(
              delegate: SliverChildBuilderDelegate(
                (_, int i) => tiles[i].values.first,
                childCount: tiles.length,
              ),
              gridDelegate: SliverStaggeredGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                staggeredTileBuilder: (int i) => tiles[i].keys.first,
                staggeredTileCount: tiles.length
              ),
            ),
          )
        ],
      )
    );
  }

  Widget _buildGridTile(Widget child, {Color color, LinearGradient gradient, bool padding: true}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
      child: Material(
        elevation: 4.0,
        shadowColor: Colors.black38,
        color: Colors.white,
        borderRadius: BorderRadius.circular(4.0),
        child: Container(
          padding: padding ? EdgeInsets.all(8.0) : null,
          decoration: BoxDecoration(
            gradient: gradient
          ),
          child: child
        )
      ),
    );
  }
}