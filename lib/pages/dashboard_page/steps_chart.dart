import 'package:flutter/material.dart';
import 'package:fcharts/fcharts.dart';
import '../../models/steps.dart';

class StepsChart extends StatelessWidget {
  final List<StepsDay> stepsData = [
    StepsDay(DateTime(2018, 5, 10), 6875),
    StepsDay(DateTime(2018, 5, 11), 4071),
    StepsDay(DateTime(2018, 5, 12), 9096),
    StepsDay(DateTime(2018, 5, 13), 13697),
    StepsDay(DateTime(2018, 5, 14), 8096),
  ];

  final int daysToShow;
  StepsChart({this.daysToShow});

  @override
  Widget build(BuildContext context) {
    List<StepsDay> truncatedStepsData = new List();

    if(daysToShow == null) {
      truncatedStepsData = stepsData;
    }
    else {
      for (int i = stepsData.length-1; i > stepsData.length - 1 - daysToShow; i--) {
        truncatedStepsData.insert(0, stepsData[i]);
      }
    }

    return BarChart<StepsDay, String, int>(
      data: truncatedStepsData,
      xAxis: ChartAxis<String>(
        span: new ListSpan(truncatedStepsData.map((steps) => steps.dateTime.toString()).toList()),
      ),
      yAxis: ChartAxis<int>(
        span: new IntSpan(0, _getMaxSteps()),
      ),
      bars: [
        new Bar<StepsDay, String, int>(
          xFn: (steps) => steps.dateTime.toString(),
          valueFn: (steps) => steps.steps,
          fill: new PaintOptions.fill(color: Colors.white),
          stack: BarStack<int>(),
        ),
      ],
    );
  }

  int _getMaxSteps() {
    int max = 0;
    for (var stepsDay in stepsData) {
      if(stepsDay.steps > max) max = stepsDay.steps;
    }
    return max;
  }
}