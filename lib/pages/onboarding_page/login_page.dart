import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SizedBox.expand(
            child: Image.asset('res/login.jpeg', fit: BoxFit.cover),
          ),
          SizedBox.expand(
            child: Container(color: Colors.black38),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('FatFit', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700, fontSize: 72.0)),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32.0),
                  child: Text('We help you become the best version of yourself and have the body of your dreams.', textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 21.0)),
                ),
                Padding(padding: EdgeInsets.only(bottom: 96.0),),
                Material(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(8.0),
                  child: InkWell(
                    onTap: () => null,
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 48.0, vertical: 32.0),
                      child: Text('START YOUR JOURNEY', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w700)),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}