import 'package:flutter/material.dart';

class OnboardingPage extends StatelessWidget {
  final String title;
  final String description;
  final String imgPath;

  const OnboardingPage(this.title, this.imgPath, this.description);

  @override
  Widget build(BuildContext context) {    
    return Column(
      children: <Widget>[
        Expanded(
          child: Image.asset(imgPath, fit: BoxFit.cover),
        ),
        Container(
          color: Colors.white,
          margin: EdgeInsets.all(32.0),
          child: Column(
            children: <Widget>[
              Text(title, textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontSize: 28.0, fontWeight: FontWeight.w600)),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(description, textAlign: TextAlign.center, style: TextStyle(color: Colors.black)),
              ),
            ],
          ),
        ),
      ],
    );
  }
}