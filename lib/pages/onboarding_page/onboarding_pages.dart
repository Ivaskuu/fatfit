import 'package:flutter/material.dart';
import 'onboarding_page.dart';

class OnboardingPages extends StatefulWidget{
  @override
  _OnboardingPagesState createState() => _OnboardingPagesState();
}

class _OnboardingPagesState extends State<OnboardingPages>{
  PageController _controller = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: PageView(
              controller: _controller,
              children: <Widget>[
                OnboardingPage('The title of this page', 'res/onboard1.jpeg', 'This is a short description of this onboarding page that should represent it'),
                OnboardingPage('The title of this page', 'res/onboard2.jpeg', 'This is a short description of this onboarding page that should represent it'),
                OnboardingPage('The title of this page', 'res/onboard3.jpeg', 'This is a short description of this onboarding page that should represent it'),
                OnboardingPage('The title of this page', 'res/onboard4.jpeg', 'This is a short description of this onboarding page that should represent it'),
                OnboardingPage('The title of this page', 'res/onboard5.jpeg', 'This is a short description of this onboarding page that should represent it'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 16.0, right: 8.0),
            child: Align(
              alignment: Alignment.centerRight,
              child: FlatButton(
                onPressed: () => _controller.nextPage(duration: Duration(milliseconds: 500), curve: Curves.easeInOut),
                child: /* _controller.page != 4 ? */ Text('NEXT') // : Text('I\'M READY'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}