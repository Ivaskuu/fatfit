import 'package:flutter/material.dart';

class CalendarWidget extends StatefulWidget {
  final int uncompletedExercises;
  CalendarWidget(this.uncompletedExercises);

  @override
  _CalendarWidgetState createState() => new _CalendarWidgetState();
}

class _CalendarWidgetState extends State<CalendarWidget> {
  @override
  Widget build(BuildContext context) {
    return new Material(
      elevation: 3.0,
      shadowColor: Colors.black12,
      color: Colors.white,
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.only(bottom: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            dayButton(DateTime.now().subtract(Duration(days: 3)), completed: true),
            dayButton(DateTime.now().subtract(Duration(days: 2))),
            dayButton(DateTime.now().subtract(Duration(days: 1)), completed: true),
            dayButton(DateTime.now()),
            dayButton(DateTime.now().add(Duration(days: 1))),
            dayButton(DateTime.now().add(Duration(days: 2))),
            dayButton(DateTime.now().add(Duration(days: 3))),
          ],
        ),
      ),
    );
  }

  Widget dayButton(DateTime date, {bool completed: false}) {    
    if(date.day == DateTime.now().day) {
      if(widget.uncompletedExercises > 0) {
        return Container(
          padding: EdgeInsets.all(12.0),
          foregroundDecoration: ShapeDecoration(
            shape: CircleBorder(side: BorderSide(color: Colors.blue)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(weekDayToShort(date)),
              Text(date.day.toString()),
            ],
          ),
        );
      }
      else {
        return Material(
          shape: CircleBorder(),
          color: Color(0xFF278CFF),
          child: Container(
            padding: EdgeInsets.all(12.0),
            child: Icon(Icons.done, color: Colors.white, size: 30.0),
          ),
        );
      }
    }
    else if(DateTime.now().isAfter(date) && completed) {
      return Icon(Icons.done, color: Color(0xFF278CFF));
    }
    else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(weekDayToShort(date), style: TextStyle(color: Colors.black54, fontSize: 12.0)),
          Text(date.day.toString(), style: TextStyle(color: Colors.black54, fontSize: 12.0)),
        ],
      );
    }
  }

  String weekDayToShort(DateTime date) {
    String weekDay;
    switch(date.weekday) {
      case 1: weekDay = 'LUN'; break;
      case 2: weekDay = 'MAR'; break;
      case 3: weekDay = 'MER'; break;
      case 4: weekDay = 'GIO'; break;
      case 5: weekDay = 'VEN'; break;
      case 6: weekDay = 'SAB'; break;
      case 7: weekDay = 'DOM'; break;
      default: weekDay = 'err'; break;
    }

    return weekDay;
  }
}