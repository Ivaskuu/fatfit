import 'package:flutter/material.dart';
import '../../models/exercise.dart';
import 'calendar_widget.dart';
import 'exercise_tile_widget.dart';

class ExercisesPage extends StatefulWidget {
  @override
  _GymPageState createState() => new _GymPageState();
}

class _GymPageState extends State<ExercisesPage> {
  List<Exercise> exercises;

  @override
  void initState() {
    super.initState();
    exercises = [
      Exercise('Corsa lenta', 'Corsa lenta', 'tapis_roulant.jpg', [
        { ExerciseProperties.Time: '20min' },
        { ExerciseProperties.Difficulty: 'Moderate' },
        { ExerciseProperties.Calories: '220cal' },
      ], completed: false),
      Exercise('Corsa veloce', 'Corsa veloce', 'tapis_roulant.jpg', [
        { ExerciseProperties.Time: '20min' },
        { ExerciseProperties.Difficulty: 'Moderate' },
        { ExerciseProperties.Calories: '220cal' },
      ]),
      Exercise('Lat Machine', 'Corsa veloce', 'lat_machine.jpeg', [
        { ExerciseProperties.Time: '20min' },
        { ExerciseProperties.Difficulty: 'Moderate' },
        { ExerciseProperties.Calories: '220cal' },
      ]),
      Exercise('Leg Extension', 'Corsa veloce', 'leg_extension.jpg', [
        { ExerciseProperties.Time: '20min' },
        { ExerciseProperties.Difficulty: 'Moderate' },
        { ExerciseProperties.Calories: '220cal' },
      ]),
      Exercise('Leg Curl', 'Corsa veloce', 'leg_curl.jpg', [
        { ExerciseProperties.Time: '20min' },
        { ExerciseProperties.Difficulty: 'Moderate' },
        { ExerciseProperties.Calories: '220cal' },
      ]),
      Exercise('Leg Press', 'Corsa veloce', 'leg_press.jpg', [
        { ExerciseProperties.Time: '20min' },
        { ExerciseProperties.Difficulty: 'Moderate' },
        { ExerciseProperties.Calories: '220cal' },
      ]),
      Exercise('Bici lento', 'Corsa veloce', 'bici.jpg', [
        { ExerciseProperties.Time: '20min' },
        { ExerciseProperties.Difficulty: 'Moderate' },
        { ExerciseProperties.Calories: '220cal' },
      ]),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: Colors.white,
                elevation: 0.0,
                forceElevated: false,
                title: Text('My Exercises', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 24.0)),
                actions: <Widget>[
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.event, color: Colors.black),
                  )
                ],
                pinned: false,
                floating: true,
              ),
              SliverToBoxAdapter(
                child: Container(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: CalendarWidget(uncompletedExercises()),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (_, int i) => i == exercises.length ? Container(margin: EdgeInsets.only(bottom: 64.0)) : ExerciseTileWidget(exercises[i], this),
                  childCount: exercises.length + 1,
                ),
              )
            ],
          ),
          exercisesFab()
        ],
      ),
    );
  }

  Widget exercisesFab() {
    if(uncompletedExercises() > 0) {
      return Align(
        alignment: Alignment.bottomLeft,
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: FloatingActionButton.extended(
            onPressed: () {},
            heroTag: 'exercises tag',
            backgroundColor: Colors.black,
            icon: Icon(Icons.assignment),
            label: Text(uncompletedExercises().toString() + ' more exercises'),
          ),
        )
      );
    }
    else {
      return Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: FloatingActionButton.extended(
            onPressed: () {},
            heroTag: 'exercises tag',
            backgroundColor: Colors.white,
            icon: Icon(Icons.done, color: Colors.black),
            label: Text('Day completed,good job!', style: TextStyle(color: Colors.black)),
          )
        )
      );
    }
  }

  int uncompletedExercises() {
    int num = 0;
    for (Exercise ex in exercises) { if(!ex.completed) num++; }

    return num;
  }
}