import 'package:flutter/material.dart';
import '../../models/exercise.dart';

class ExerciseTileWidget extends StatefulWidget {
  final Exercise e;
  final State ePageState;
  ExerciseTileWidget(this.e, this.ePageState);
  
  @override
  _ExerciseTileWidgetState createState() => _ExerciseTileWidgetState();
}

class _ExerciseTileWidgetState extends State<ExerciseTileWidget> {
  @override
  Widget build(BuildContext context) {
    if(!widget.e.completed) {
      return InkWell(
        onTap: () => showDialog(context: context, builder: (_) => ExerciseDialog(widget.e, this)),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
          child: Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 32.0, top: 4.0, bottom: 4.0),
                child: Material(
                  color: Colors.white,
                  elevation: 4.0,
                  shadowColor: Colors.black26,
                  borderRadius: BorderRadius.circular(4.0),
                  child: Container(
                    margin: EdgeInsets.only(left: 84.0),
                    padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 28.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Text(widget.e.name, maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(color: Color(0xFF278CFF), fontWeight: FontWeight.w700, fontSize: 20.0)),
                              Padding(padding: EdgeInsets.only(bottom: 6.0)),
                              Text('3 sets / 12 reps', maxLines: 3, overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 14.0)),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(8.0),
                          child: Icon(Icons.play_arrow, color: Color(0xFF278CFF)),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: SizedBox.fromSize(
                  size: Size(110.0, 90.0),
                    child: Material(
                    elevation: 4.0,
                    shadowColor: Colors.black45,
                    borderRadius: BorderRadius.circular(4.0),
                    child: Image.asset('res/${widget.e.img}', fit: BoxFit.cover, alignment: Alignment.center),
                  ),
                ),
              )
            ],
          )
        ),
      );
    }
    else {
      return new InkWell(
        onTap: () => showDialog(context: context, builder: (_) => ExerciseDialog(widget.e, this)),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 3.0),
          child: Material(
            color: Color(0xFF278CFF),
            borderRadius: BorderRadius.circular(3.0),
            elevation: 2.0,
            shadowColor: Colors.black54,
            child: Container(
              margin: EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(widget.e.name, maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18.0)),
                      Padding(padding: EdgeInsets.only(bottom: 2.0)),
                      Text('Burned 357 kcal', maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.white)),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.all(8.0),
                    child: Icon(Icons.done, color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }
  }

  void updateParent() {
    widget.ePageState.setState(() {});
  }
}

class ExerciseDialog extends StatelessWidget {
  final Exercise e;
  final _ExerciseTileWidgetState eState;
  ExerciseDialog(this.e, this.eState);

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.zero,
      titlePadding: EdgeInsets.zero,
      children: <Widget>[
        SizedBox.fromSize(
          size: Size.fromHeight(200.0),
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('res/${e.img}'),
                fit: BoxFit.cover
              )
            ),
          )
        ),
        Container(
          margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0, bottom: 16.0),
          child: Text(e.name, textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20.0)),
        ),
        Container(
          margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
          child: Text('Consequat do amet ipsum minim Lorem esse magna minim. Aliqua ut consequat nisi tempor laboris ipsum adipisicing reprehenderit amet nostrud excepteur ex exercitation.', textAlign: TextAlign.center, style: TextStyle()),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: e.properties.map<Widget>((property) => _buildInfoIconText(property)).toList(),
        ),
        Container(
          padding: EdgeInsets.all(16.0),
          child: Material(
            elevation: 10.0,
            color: Color(0xFF278CFF),
            shadowColor: Color(0xFF278CFF),
            borderRadius: BorderRadius.circular(4.0),
            child: InkWell(
              onTap: () {
                eState.updateParent();
                eState.setState(() => e.completed = !e.completed);
                Navigator.pop(context);
              },
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(e.completed ? 'DONE' : 'SET AS DONE', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                    Icon(Icons.done, color: Colors.white),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildInfoIconText(Map<ExerciseProperties, dynamic> e) {
    IconData icon;
    switch(e.keys.first) {
      case ExerciseProperties.Calories: icon = Icons.network_check; break;
      case ExerciseProperties.Time: icon = Icons.timer; break;
      case ExerciseProperties.Difficulty: icon = Icons.equalizer; break;
      case ExerciseProperties.Calories: icon = Icons.network_check; break;
      default: icon = Icons.error_outline; break;
    }
    
    return Container(
      margin: EdgeInsets.all(12.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(icon),
          Text(e.values.first.toString())
        ],
      ),
    );
  }
}