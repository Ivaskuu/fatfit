import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'dart:math';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  ScrollController _scrollController;
  int _backgroundOpacity = 0;
  
  @override
  void initState()
  {
    super.initState();
    _controller = AnimationController(vsync: this, duration: Duration(seconds: 10));
    _controller.addListener(() => setState(() {}));
    _controller.addStatusListener((status) {
      if(status == AnimationStatus.completed) _controller.reverse();
      else if(status == AnimationStatus.dismissed) _controller.forward();
    });
    _controller.forward();

    // Scroll controller
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if(_backgroundOpacity <= 255) {
        setState(() {
          _backgroundOpacity = min(255, (255 * (_scrollController.offset / (MediaQuery.of(context).size.height * 0.5))).round());
        });
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _buildFulscreenImage(),
          SizedBox.expand(
            child: Container(
              color: Colors.white.withAlpha(_backgroundOpacity),
            ),
          ),
          _buildProfileList(context),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0.0,
              ),
            ],
          ),
        ],
      ),
    );
  }

  ListView _buildProfileList(BuildContext context) {
    return ListView(
      controller: _scrollController,
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.5)),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 16.0),
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 44.0,
                backgroundImage: AssetImage('res/selfie.jpeg'),
              ),
            ), // User image
            Container(
              margin: EdgeInsets.only(bottom: 32.0),
              child: Stack(
                children: [
                  Positioned(
                    top: 2.0,
                    left: 2.0,
                    child: new Text(
                      'JESSICA CHARLES',
                      style: TextStyle(fontSize: 34.0, fontWeight: FontWeight.w700, color: Colors.white).copyWith(color: Colors.black.withOpacity(0.1)),
                    ),
                  ),
                  BackdropFilter(
                    filter: new ui.ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                    child: new Text('JESSICA CHARLES', style: TextStyle(fontSize: 34.0, fontWeight: FontWeight.w700, color: Colors.white)),
                  ),
                ],
              ),
            ), // User name with shadow
            Container(
              margin: EdgeInsets.only(bottom: 64.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.whatshot, color: Colors.amber, size: 26.0),
                  Icon(Icons.whatshot, color: Colors.amber, size: 26.0),
                  Icon(Icons.whatshot, color: Colors.amber, size: 26.0),
                  Icon(Icons.whatshot, color: Colors.white, size: 26.0),
                  Icon(Icons.whatshot, color: Colors.white, size: 26.0),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 16.0),
              child: Icon(Icons.keyboard_arrow_down, color: Colors.white, size: 48.0)
            ),
          ],
        ), // Infos
        Padding(
          padding: const EdgeInsets.all(64.0),
          child: Center(child: Text('TODO')),
        ),
        Padding(
          padding: const EdgeInsets.all(64.0),
          child: Center(child: Text('TODO')),
        ),
        Padding(
          padding: const EdgeInsets.all(64.0),
          child: Center(child: Text('TODO')),
        ),
        Padding(
          padding: const EdgeInsets.all(64.0),
          child: Center(child: Text('TODO')),
        ),
        Padding(
          padding: const EdgeInsets.all(64.0),
          child: Center(child: Text('TODO')),
        ),
        Padding(
          padding: const EdgeInsets.all(64.0),
          child: Center(child: Text('TODO')),
        ),
      ],
    );
  }

  Stack _buildFulscreenImage() {
    return Stack(
      children: <Widget>[
        SizedBox.expand(
          child: Image.asset('res/selfie.jpeg', fit: BoxFit.cover, alignment: Alignment(0.2, 0.0)),
        ), // Fullscreen image
        SizedBox.expand(
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.transparent,
                  ColorTween(begin: Colors.indigo[700], end: Colors.red).animate(CurvedAnimation(parent: _controller, curve: Curves.easeInOut)).value
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter
              )
            ),
          ),
        ), // Gradient
      ]
    );
  }

  //This is an Easter egg
  // 8====D> - - - -
}