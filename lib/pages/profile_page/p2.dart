import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => new _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Image.asset('res/login.jpeg', fit: BoxFit.cover),
              Container(color: Colors.black45),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 16.0),
                          child: Text('Steve Ganz', textAlign: TextAlign.end, style: TextStyle(color: Colors.white, fontSize: 32.0, fontWeight: FontWeight.bold)),
                        )
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 32.0)),
                      SizedBox.fromSize(
                        size: Size.fromHeight(256.0),
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            new Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SizedBox.fromSize(
                                size: Size.fromWidth(170.0),
                                child: Material(
                                  color: Colors.white,
                                  elevation: 10.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
          Center(
            child: Hero(
              tag: 'Profile image',
              child: Material(
                shape: CircleBorder(),
                child: Image.asset('res/login.jpeg'),
              ),
            )
          )
        ],
      )
    );
  }
}