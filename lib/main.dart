import 'package:flutter/material.dart';
import 'pages/main_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fatfit',
      theme: ThemeData(primaryColor: Colors.black, accentColor: Colors.black12),
      home: MainPage(),
    );
  }
}
