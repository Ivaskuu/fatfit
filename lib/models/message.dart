class ChatMessage
{
  String userName;
  String content;
  
  ChatMessage(this.userName, this.content);
}

class ImageMessage
{
  String userName;
  String imgUrl;

  ImageMessage(this.userName, this.imgUrl);
}