class Food
{
  String name;
  double kcalGr; // Per 100 grams
  String img;

  int weight;
  double carbs;
  double fats;
  double proteins;

  Food(this.name, this.kcalGr, {this.img, this.weight, this.carbs, this.fats, this.proteins})
  {
    // 1g fat = 9kcal / 1g carbs / proteins = 4kcal

    // In case of missing nutritional values:
    // generally, there are more carbs, then fats and finally proteins,
    // so I decided that 3x carbs, 2x fats and 1x proteins

    if(carbs != null && fats != null && proteins != null)
    {
      // TODO: Check that nutritional values are correct?
    }
    else if(carbs == null && fats == null && proteins == null)
    {
      double oneSixth = kcalGr / 6;
      carbs = (3 * oneSixth) / 4;
      fats = (2 * oneSixth) / 9;
      proteins = (1 * oneSixth) / 4;
    }
    else
    {
      if(carbs != null) kcalGr -= carbs * 4;
      if(fats != null) kcalGr -= fats * 9;
      if(proteins != null) kcalGr -= proteins * 4;
    }
  }
}