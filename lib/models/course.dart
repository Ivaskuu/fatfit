class Course
{
  String name;
  String description;
  double durationMin; // in minutes
  double kcalHour;
  List<CourseDate> date;

  Course(this.name, this.description, this.durationMin, this.kcalHour, this.date);
}

class CourseDate
{
  int day; // 0 (monday) - 7 (sunday)
  int hour;
  int minutes;

  CourseDate(this.day, this.hour, {this.minutes: 0});
}