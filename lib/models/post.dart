import 'user.dart';
import 'comment.dart';

enum PostTag { Trophie, Cook, Proggress, Selfie, Bodie, Motivational, Question }

class Post {
  User user;
  String content;
  int likes;
  List<Comment> comments;
  DateTime dateTime;
  PostTag postTag;
  bool fake;

  Post.newPost(this.user, this.content, this.likes, this.postTag, {this.fake}) {
    comments = List();
    dateTime = DateTime.now();
  }

  Post.award(this.user, this.content, this.likes) {
    postTag = PostTag.Trophie;

    comments = List();
    dateTime = DateTime.now();
  }

  Post(this.user, this.content, this.comments, this.dateTime, this.postTag);
}

class ImagePost extends Post {
  String imgUrl;

  ImagePost.newPost(User user, this.imgUrl, int likes, PostTag postTag, {String content, bool fake: false})
      : super.newPost(user, content, likes, postTag, fake: fake);
  ImagePost(User user, String content, this.imgUrl, List<Comment> comments,
      DateTime dateTime, PostTag postTag)
      : super(user, content, comments, dateTime, postTag);
}
