class StepsDay
{
  final DateTime dateTime;
  final int steps;

  StepsDay(this.dateTime, this.steps);
}