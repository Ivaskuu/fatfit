class Trophie
{
  String title;
  String description;
  int difficulty;

  Trophie(this.title, this.description, this.difficulty);
}