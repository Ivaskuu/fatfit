enum ExerciseProperties { Calories, Time, Sets, Reps, Difficulty }

class Exercise
{
  String name;
  String description;
  String img;
  List<Map<ExerciseProperties, dynamic>> properties;
  bool completed;

  Exercise(this.name, this.description, this.img, this.properties, {this.completed: false});
}