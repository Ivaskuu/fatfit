import 'user.dart';

class Comment
{
  User user;
  String content;
  Comment subComment;

  Comment(this.user, this.content, {this.subComment});
}