class Equipment
{
  String name;
  String description;
  List<String> muscles;

  Equipment(this.name, this.description, this.muscles);
}