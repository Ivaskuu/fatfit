import 'post.dart';
import 'trophie.dart';

class User
{
  // Person infos
  String firstName;
  String lastName;
  int birthYear;
  bool isTrainer;

  // Physical characteristics
  double initialWeight;
  double actualweight;
  int height; // cm

  // Social infos
  String userName;
  String profileImgUrl;
  int experience;
  int level;
  List<Post> posts;
  List<Trophie> unlockedTrophies;

  User.newUser(this.firstName, this.lastName, this.birthYear, this.initialWeight, this.height,
    this.userName, {this.isTrainer: false, this.profileImgUrl})
  {
    actualweight = initialWeight;
    experience = 0;
    level = 1;
    posts = List<Post>();
    unlockedTrophies = List();
  }

  User(this.firstName, this.lastName, this.birthYear, this.initialWeight, this.actualweight, this.height, this.userName,
    this.level, this.experience, this.posts, this.unlockedTrophies, this.isTrainer, {this.profileImgUrl});
}