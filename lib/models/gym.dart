import 'equipment.dart';
import 'course.dart';

class Gym
{
  List<Equipment> equipments;
  List<Course> courses;

  Gym(this.equipments, this.courses);
}